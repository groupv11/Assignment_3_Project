# Java Assignment 3

## Contents

[Information](#Information)

[Install](#Install)

[Usage](#Usage)

[Maintainers](#Maintainers)

[Licence](#Licence) 


## Information 

This is an application used for performing changes and queries in a Movie database. You can use swagger to test the program.


### Project structure
```
`-- src
    |-- main
    |   |-- java
    |   |   `-- com
    |   |       `-- example
    |   |           `-- assignment_3
    |   |               |-- Assignment3Application.java
    |   |               |-- controllers
    |   |               |   |-- FranchiseController.java
    |   |               |   |-- MovieController.java
    |   |               |   `-- PartController.java
    |   |               |-- mappers
    |   |               |   |-- FranchiseMapper.java
    |   |               |   |-- MovieMapper.java
    |   |               |   `-- PartMapper.java
    |   |               |-- models
    |   |               |   |-- Franchise.java
    |   |               |   |-- Movie.java
    |   |               |   |-- Part.java
    |   |               |   `-- dtos
    |   |               |       |-- franchise
    |   |               |       |   |-- FranchiseDTO.java
    |   |               |       |   `-- FranchisePostDTO.java
    |   |               |       |-- movie
    |   |               |       |   |-- MovieDTO.java
    |   |               |       |   |-- MoviePostDTO.java
    |   |               |       |   `-- MoviePutDTO.java
    |   |               |       `-- part
    |   |               |           |-- PartDTO.java
    |   |               |           `-- PartPostDTO.java
    |   |               |-- repositories
    |   |               |   |-- FranchiseRepository.java
    |   |               |   |-- MovieRepository.java
    |   |               |   `-- PartRepository.java
    |   |               `-- services
    |   |                   |-- CrudService.java
    |   |                   |-- franchise
    |   |                   |   |-- FranchiseService.java
    |   |                   |   `-- FranchiseServiceImpl.java
    |   |                   |-- movie
    |   |                   |   |-- MovieService.java
    |   |                   |   `-- MovieServiceImpl.java
    |   |                   `-- part
    |   |                       |-- PartService.java
    |   |                       `-- PartServiceImpl.java
    |   `-- resources
    |       |-- application.properties
    |       `-- data.sql
    `-- test
        `-- java
            `-- com
                `-- example
                    `-- assignment_3
                        `-- Assignment3ApplicationTests.java


```


## Install
Clone git repo - open in IDE
- open pgAdmin and create database MovieSystem.

## Usage
run 'Assignment3Application' and open http://localhost:8081/swagger-ui/index.html to test 

## Maintainers
Karoline Øijorden, Ulrik Lunde and Vegard Gunnarson

## Licence
Open
