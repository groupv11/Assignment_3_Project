INSERT INTO franchise ("name", "description") VALUES ('Marvel', 'Marvel Cinematic Universe');
INSERT INTO franchise ("name", "description") VALUES ('The lord of the rings', 'The lord of the cinematic universe');
INSERT INTO franchise ("name", "description") VALUES ('Avatar', 'The avatar');
INSERT INTO movie ("title", "year", "director", "picture", "trailer", "genre", "franchise_id") VALUES ('Spiderman: Homecoming', '2017', 'Vegard Gunnarson', 'https://www.google.com/imgres?imgurl=https%3A%2F%2Fsfanytime-images-prod-http2.secur[…]nt=safari&ved=2ahUKEwiahe-IlvH5AhVnoosKHZnPDBsQMygAegUIARDGAQ', 'https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=video&cd=&cad=rja&uact=8&ved=[…]b.com%2Ftitle%2Ftt2250912%2F&usg=AOvVaw0Qz7WSY734I33O4F3JYiCP', 'Action', 1);
INSERT INTO movie ("title", "year", "director", "picture", "trailer", "genre", "franchise_id") VALUES ('Spiderman: Far from home', '2017', 'Vegard Gunnarson', 'https://www.google.com/imgres?imgurl=https%3A%2F%2Fsfanytime-images-prod-http2.secur[…]nt=safari&ved=2ahUKEwiahe-IlvH5AhVnoosKHZnPDBsQMygAegUIARDGAQ', 'https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=video&cd=&cad=rja&uact=8&ved=[…]b.com%2Ftitle%2Ftt2250912%2F&usg=AOvVaw0Qz7WSY734I33O4F3JYiCP', 'Action', 1);
INSERT INTO movie ("title", "year", "director", "picture", "trailer", "genre", "franchise_id") VALUES ('The lord of the rings: The fellowship of the ring', '2017', 'Vegard Gunnarson', 'https://www.google.com/imgres?imgurl=https%3A%2F%2Fsfanytime-images-prod-http2.secur[…]nt=safari&ved=2ahUKEwiahe-IlvH5AhVnoosKHZnPDBsQMygAegUIARDGAQ', 'https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=video&cd=&cad=rja&uact=8&ved=[…]b.com%2Ftitle%2Ftt2250912%2F&usg=AOvVaw0Qz7WSY734I33O4F3JYiCP', 'Action', 2);
INSERT INTO movie ("title", "year", "director", "picture", "trailer", "genre", "franchise_id") VALUES ('The lord of the rings: The return of the king', '2017', 'Vegard Gunnarson', 'https://www.google.com/imgres?imgurl=https%3A%2F%2Fsfanytime-images-prod-http2.secur[…]nt=safari&ved=2ahUKEwiahe-IlvH5AhVnoosKHZnPDBsQMygAegUIARDGAQ', 'https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=video&cd=&cad=rja&uact=8&ved=[…]b.com%2Ftitle%2Ftt2250912%2F&usg=AOvVaw0Qz7WSY734I33O4F3JYiCP', 'Action', 2);
INSERT INTO movie ("title", "year", "director", "picture", "trailer", "genre", "franchise_id") VALUES ('Avatar', '2017', 'Vegard Gunnarson', 'https://www.google.com/imgres?imgurl=https%3A%2F%2Fsfanytime-images-prod-http2.secur[…]nt=safari&ved=2ahUKEwiahe-IlvH5AhVnoosKHZnPDBsQMygAegUIARDGAQ', 'https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=video&cd=&cad=rja&uact=8&ved=[…]b.com%2Ftitle%2Ftt2250912%2F&usg=AOvVaw0Qz7WSY734I33O4F3JYiCP', 'Action', 3);
INSERT INTO part ("name", "alias", "gender", "picture") VALUES ('Spiderman', 'Peter Parker', 'Male', 'https://mubi.com/cast/tom-holland-38d2b385-a297-48bd-b5e0-b21fc7f1f675');
INSERT INTO part ("name", "alias", "gender", "picture") VALUES ('Frodo', 'Frode', 'Male', 'https://mubi.com/cast/tom-holland-38d2b385-a297-48bd-b5e0-b21fc7f1f675');
INSERT INTO part ("name", "alias", "gender", "picture") VALUES ('Neytiri', 'Avatar', 'Female', 'https://mubi.com/cast/tom-holland-38d2b385-a297-48bd-b5e0-b21fc7f1f675');
INSERT INTO movie_parts ("movie_id", "part_id") VALUES (1,1);
INSERT INTO movie_parts ("movie_id", "part_id") VALUES (1,2);
INSERT INTO movie_parts ("movie_id", "part_id") VALUES (2,1);
INSERT INTO movie_parts ("movie_id", "part_id") VALUES (3,2);
INSERT INTO movie_parts ("movie_id", "part_id") VALUES (4,2);
INSERT INTO movie_parts ("movie_id", "part_id") VALUES (5,3);





