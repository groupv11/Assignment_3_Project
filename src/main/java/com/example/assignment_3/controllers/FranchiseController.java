package com.example.assignment_3.controllers;

import com.example.assignment_3.mappers.FranchiseMapper;
import com.example.assignment_3.mappers.MovieMapper;
import com.example.assignment_3.mappers.PartMapper;
import com.example.assignment_3.models.Franchise;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.Part;
import com.example.assignment_3.models.dtos.franchise.FranchiseDTO;
import com.example.assignment_3.models.dtos.franchise.FranchisePostDTO;
import com.example.assignment_3.repositories.FranchiseRepository;
import com.example.assignment_3.services.franchise.FranchiseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "api/v1/franchise")
public class FranchiseController {
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final MovieMapper movieMapper;
    private final PartMapper partMapper;

    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, MovieMapper movieMapper, PartMapper partMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieMapper = movieMapper;
        this.partMapper = partMapper;
    }

    @Operation(summary = "Get franchise by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))}),
            @ApiResponse(responseCode = "400",
                    description = "malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @GetMapping("/getFranc/{id}")
    public ResponseEntity getById(@PathVariable int id){
        FranchiseDTO franchise = franchiseMapper.franchiseToFranchiseDto(
                franchiseService.findById(id)
        );
        return ResponseEntity.ok(franchise);
    }

    @Operation(summary = "Get all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))}),
            @ApiResponse(responseCode = "400",
                    description = "malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @GetMapping("/getAll")
    public ResponseEntity getAll(){
        Collection<FranchiseDTO> franchises = franchiseMapper.franchiseToFranchiseDto(
                franchiseService.findAll()
        );
        return ResponseEntity.ok(franchises);
    }

    @Operation(summary = "Add a franchise to database")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "201",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @PostMapping
    public ResponseEntity add(@RequestBody FranchisePostDTO franchisePostDTO){
        Franchise f = franchiseMapper.franchisePostDtoToFranchise(franchisePostDTO);
        franchiseService.add(f);
        URI location = URI.create("franchises/" + f.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update a franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @PutMapping("/updateFranc/{id}")
    public ResponseEntity update(@RequestBody FranchiseDTO franchiseDto, @PathVariable int id){
        if(id != franchiseDto.getId() || !franchiseService.exists(id))
            return ResponseEntity.badRequest().build();
        franchiseService.update(franchiseMapper.franchiseDtoToFranchise(franchiseDto));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "List all movies in franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "no such franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @GetMapping("/getMovies/{id}")
    public ResponseEntity getMoviesInFranchise(@PathVariable int id){
        Franchise f = franchiseService.findById(id);
        Set<Movie> movies = f.getMovies();
        return ResponseEntity.ok(movies.stream().map(s -> movieMapper.movieToMovieDto(s)));
    }

    @Operation(summary = "Get all parts in a franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "no such franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @GetMapping("/getPartsInFranchise/{id}")
    public ResponseEntity getAllPartsInFranchise(@PathVariable int id){
        Franchise f = franchiseService.findById(id);
        Set<Movie> movies = f.getMovies();
        Set<Part> parts = new HashSet<>();
        for(Movie m : movies){
            parts.addAll(m.getParts());
        }
        return ResponseEntity.ok(parts.stream().map(s -> partMapper.partToPartDto(s)));
    }

    @Operation(summary = "Update movies in franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "201",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "no such movie",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "no such franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @PutMapping("/updateMovies/{id}")
    public ResponseEntity updateMoviesInFranchise(@RequestBody int[] movieIds, @PathVariable int id){
        franchiseService.updateAllMovies(movieIds, id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete franchise from universe")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "201",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "no such franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @DeleteMapping("/deleteById/{id}")
    public ResponseEntity deleteById(@PathVariable int id){
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
