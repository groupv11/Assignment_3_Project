package com.example.assignment_3.controllers;

import com.example.assignment_3.mappers.MovieMapper;
import com.example.assignment_3.mappers.PartMapper;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.Part;
import com.example.assignment_3.models.dtos.movie.MovieDTO;
import com.example.assignment_3.models.dtos.movie.MoviePostDTO;
import com.example.assignment_3.models.dtos.movie.MoviePutDTO;
import com.example.assignment_3.services.franchise.FranchiseService;
import com.example.assignment_3.services.movie.MovieService;
import com.example.assignment_3.services.part.PartService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "api/v1/movie")
public class MovieController {
    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final FranchiseService franchiseService;
    private final PartMapper partMapper;
    private final PartService partService;

    public MovieController(MovieService movieService, MovieMapper movieMapper, FranchiseService franchiseService, PartMapper partMapper, PartService partService) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.franchiseService = franchiseService;
        this.partMapper = partMapper;
        this.partService = partService;
    }

    @Operation(summary = "Get movie by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class)))})
    })
    @GetMapping("/getmovieId/{id}")
    public ResponseEntity getById(@PathVariable int id){
        MovieDTO movie = movieMapper.movieToMovieDto(
                movieService.findById(id)
        );
        return ResponseEntity.ok(movie);
    }

    @Operation(summary = "Get all movies")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "malformed requeset",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "no such movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @GetMapping("/getMovie")
    public ResponseEntity getAll(){
        Collection<MovieDTO> movies = movieMapper.movieToMovieDto(
                movieService.findAll()
        );
        return ResponseEntity.ok(movies);
    }

    @Operation(summary = "Add a movie to database")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "201",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "malformed requeset",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "no such movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @PostMapping("/addMovie")
    public ResponseEntity add(@RequestBody MoviePostDTO moviePostDTO){
        Movie m = movieMapper.moviePostDtoToMovie(moviePostDTO);
        movieService.add(m);
        URI location = URI.create("movies/" + m.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update a movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @PutMapping("/updateMovie/{id}")
    public ResponseEntity update(@RequestBody MoviePutDTO moviePutDTO, @PathVariable int id){
        Movie m = movieMapper.moviePutDtoToMovie(moviePutDTO);
        if(id != m.getId() || !movieService.exists(id))
            return ResponseEntity.badRequest().build();
        movieService.update(m);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get all parts in movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "malformed requeset",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "no such movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @GetMapping("/getPartsinMovie/{id}")
    public ResponseEntity getPartsInMovie(@PathVariable int id){
        Movie m = movieService.findById(id);
        Set<Part> parts = m.getParts();

        return ResponseEntity.ok(parts.stream().map(s -> partMapper.partToPartDto(s)));
    }

    @Operation(summary = "Delete movie from universe")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "malformed requeset",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "no such movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @DeleteMapping("/deletemovie/{id}")
    public ResponseEntity deleteById(@PathVariable int id){
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Update parts in movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "malformed requeset",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "no such movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @PutMapping("/updateParts/{id}")
    public ResponseEntity updatePartsInMovie(@RequestBody int[] partIds, @PathVariable int id){
        Set<Part> newParts = new HashSet<>();
        for(Integer partId : partIds){
            newParts.add(partService.findById(partId));
        }
        movieService.updateParts(newParts, id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Update all Parts in a movie")
    @PutMapping("/updateMovies/{id}")
    public ResponseEntity updateMoviesInFranchise(@RequestBody int[] partIds, @PathVariable int id){
        movieService.updateAllParts(partIds, id);
        return ResponseEntity.noContent().build(); //TODO FIX METHOD NAME
    }

}
