package com.example.assignment_3.controllers;

import com.example.assignment_3.mappers.PartMapper;
import com.example.assignment_3.models.Part;
import com.example.assignment_3.models.dtos.part.PartDTO;
import com.example.assignment_3.models.dtos.part.PartPostDTO;
import com.example.assignment_3.services.part.PartService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/part")
public class PartController {

    private final PartService partService;
    private final PartMapper  partMapper;

    public PartController(PartService partService, PartMapper partMapper) {
        this.partService = partService;
        this.partMapper = partMapper;
    }
    @Operation(summary = "Get part by id")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "malformed requeset",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "no such part",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @GetMapping("/getPartbyId/{id}")
    public ResponseEntity getById(@PathVariable int id){
        PartDTO part = partMapper.partToPartDto(
                partService.findById(id)
        );
        return ResponseEntity.ok(part);
    }
    @Operation(summary = "Get all parts")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "malformed requeset",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "no such part",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @GetMapping("/getMovies")
    public ResponseEntity getAll(){
        Collection<PartDTO> parts = partMapper.partToPartDto(
                partService.findAll()
        );
        return ResponseEntity.ok(parts);
    }

    @Operation(summary = "Add a part to database")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "201",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "malformed requeset",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "no such part",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @PostMapping("/addParttoDatabase")
    public ResponseEntity add(@RequestBody PartPostDTO partPostDTO){
        Part p = partMapper.partPostDtoToPart(partPostDTO);
        partService.add(p);
        URI location = URI.create("parts/" + p.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update a part")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Part successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @PutMapping("/updatePart/{id}")
    public ResponseEntity update(@RequestBody PartDTO partDto, @PathVariable int id){
        if(id != partDto.getId() || !partService.exists(id))
            return ResponseEntity.badRequest().build();
        partService.update(partMapper.partDtoToPart(partDto));
        return ResponseEntity.noContent().build();
    }
    @Operation(summary = "Delete part")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "success",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "malformed requeset",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "no such part",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @DeleteMapping("/deletePart/{id}")
    public ResponseEntity deleteById(@PathVariable int id){
        partService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
