package com.example.assignment_3.mappers;

import com.example.assignment_3.models.Part;
import com.example.assignment_3.models.dtos.part.PartDTO;
import com.example.assignment_3.models.dtos.part.PartPostDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface PartMapper {
    PartDTO partToPartDto(Part part);
    @Mapping(target = "name")
    Part partDtoToPart(PartDTO partDTO);
    Part partPostDtoToPart(PartPostDTO partPostDTO);
    Collection<PartDTO> partToPartDto(Collection<Part> parts);


}
