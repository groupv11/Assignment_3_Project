package com.example.assignment_3.mappers;

import com.example.assignment_3.models.Franchise;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.dtos.movie.MovieDTO;
import com.example.assignment_3.models.dtos.movie.MoviePostDTO;
import com.example.assignment_3.models.dtos.movie.MoviePutDTO;
import com.example.assignment_3.services.franchise.FranchiseService;
import com.example.assignment_3.services.movie.MovieService;
import com.example.assignment_3.services.part.PartService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Autowired
    MovieService movieService;
    @Autowired
    FranchiseService franchiseService;
    @Autowired
    PartService partService;

    @Mapping(target = "franchise_id", source = "franchise.id")
    public abstract MovieDTO movieToMovieDto(Movie movie);
    public abstract Movie movieDtoToMovie(MovieDTO movieDTO);
    public abstract Movie moviePostDtoToMovie(MoviePostDTO moviePostDTO);
    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies);
    public abstract Movie moviePutDtoToMovie(MoviePutDTO moviePutDTO);

    @Named("franchiseIdToFranchise")
    Franchise mapIdToFranchise(int id){ return franchiseService.findById(id);}

}
