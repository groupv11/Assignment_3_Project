package com.example.assignment_3.mappers;

import com.example.assignment_3.models.Franchise;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.dtos.franchise.FranchiseDTO;
import com.example.assignment_3.models.dtos.franchise.FranchisePostDTO;
import com.example.assignment_3.models.dtos.movie.MovieDTO;
import com.example.assignment_3.services.franchise.FranchiseService;
import com.example.assignment_3.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    public abstract FranchiseDTO franchiseToFranchiseDto(Franchise franchise);
    @Mapping(target = "name")
    public abstract Franchise franchiseDtoToFranchise(FranchiseDTO franchiseDTO);
    public abstract Franchise franchisePostDtoToFranchise(FranchisePostDTO franchisePostDTO);
    public abstract Collection<FranchiseDTO> franchiseToFranchiseDto(Collection<Franchise> franchises);
}
