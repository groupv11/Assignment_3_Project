package com.example.assignment_3.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Part {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 30, nullable = false)
    private String name;

    @Column(length = 30)
    private String alias;

    @Column(length = 20)
    private String gender;

    @Column(length = 254)
    private String picture;

    @ManyToMany(mappedBy = "parts")
    private Set<Movie> movies;
}
