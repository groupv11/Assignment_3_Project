package com.example.assignment_3.models.dtos.part;

import lombok.Data;

@Data
public class PartPostDTO {
    private String name;
    private String alias;
    private String gender;
    private String picture;

}
