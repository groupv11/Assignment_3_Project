package com.example.assignment_3.models.dtos.part;

import lombok.Data;

@Data
public class PartDTO {
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String picture;

}
