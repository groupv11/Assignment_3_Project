package com.example.assignment_3.models.dtos.movie;

import lombok.Data;

@Data
public class MoviePutDTO {
    private int id;
    private String director;
    private String genre;
    private String picture;
    private String title;
    private String trailer;
    private int year;
}
