package com.example.assignment_3.models.dtos.franchise;

import lombok.Data;

@Data
public class FranchisePostDTO {
    private String name;
    private String description;
}
