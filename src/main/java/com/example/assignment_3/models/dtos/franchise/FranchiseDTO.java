package com.example.assignment_3.models.dtos.franchise;

import lombok.Data;

@Data
public class FranchiseDTO {
    private int id;
    private String name;
    private String description;
}
