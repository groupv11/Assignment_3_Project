package com.example.assignment_3.models.dtos.movie;

import com.example.assignment_3.models.Franchise;
import lombok.Data;

@Data
public class MovieDTO {
    private int id;
    private String director;
    private String genre;
    private String picture;
    private String title;
    private String trailer;
    private int year;
    private int franchise_id;

}
