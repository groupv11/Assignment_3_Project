package com.example.assignment_3.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PartNotFoundException extends RuntimeException {
    public PartNotFoundException(int id) {super("Part does not exist with ID" + id);}

}
