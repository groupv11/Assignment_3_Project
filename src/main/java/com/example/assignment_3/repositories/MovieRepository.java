package com.example.assignment_3.repositories;

import com.example.assignment_3.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository (DAO) for the movie domain class
 */
@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
}
