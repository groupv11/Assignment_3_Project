package com.example.assignment_3.repositories;

import com.example.assignment_3.models.Part; //NEVER REMOVE!
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository (DAO) for the character domain class
 */
@Repository
public interface PartRepository extends JpaRepository<Part, Integer> {
}
