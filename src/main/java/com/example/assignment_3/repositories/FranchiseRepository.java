package com.example.assignment_3.repositories;

import com.example.assignment_3.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository (DAO) for the franchise domain class
 */
@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {
}
