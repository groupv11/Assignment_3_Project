package com.example.assignment_3.services.movie;

import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.Part;
import com.example.assignment_3.services.CrudService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Set;

/**
 * Service for the movie domain class
 * Providing basic CRUD functionality
 */
public interface MovieService extends CrudService<Movie, Integer> {
    /**
     * Checks if movie exists
     * @param id movie_id
     * @return true||false
     */
    boolean exists(int id);
    public void updateAllParts(int [] partIds, int id);
    void updateParts(Set<Part> newParts, int id);
}
