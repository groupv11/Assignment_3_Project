package com.example.assignment_3.services.movie;

import com.example.assignment_3.exceptions.MovieNotFoundException;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.Part;
import com.example.assignment_3.repositories.MovieRepository;
import com.example.assignment_3.repositories.PartRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of the movie service.
 * Uses the movie repository to interact with the data store.
 */
@Service
public class MovieServiceImpl implements MovieService{

    private final MovieRepository movieRepository;
    private final PartRepository partRepository;

    public MovieServiceImpl(MovieRepository movieRepository, PartRepository partRepository) {
        this.movieRepository = movieRepository;

        this.partRepository = partRepository;
    }

    @Override
    public Movie findById(Integer id) {
        if(movieRepository.findById(id) == null){
            throw new MovieNotFoundException(id);
        }
        return movieRepository.findById(id).get();
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        Movie m = movieRepository.findById(id).get();
        movieRepository.delete(m);
    }

    @Override
    public boolean exists(int id) {
        return movieRepository.existsById(id);
    }

    @Override
    public void updateParts(Set<Part> newParts, int id) {

        Movie m = movieRepository.findById(id).get();
        m.setParts(null);
    }

    @Override
    public void updateAllParts(int [] partIds, int id){
        Movie movie = movieRepository.findById(id).get();
        Set<Part> parts = new HashSet<>();
        for(Integer ids : partIds){
            Part p = partRepository.findById(ids).get();
            Set<Movie> movies = p.getMovies();
            movies.add(movie);
            p.setMovies(movies);
            partRepository.save(p);
            parts.add(p);
        }
        movie.setParts(parts);
        movieRepository.save(movie);
    }
}
