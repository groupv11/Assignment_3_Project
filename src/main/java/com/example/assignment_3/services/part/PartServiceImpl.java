package com.example.assignment_3.services.part;

import com.example.assignment_3.exceptions.MovieNotFoundException;
import com.example.assignment_3.exceptions.PartNotFoundException;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.Part;
import com.example.assignment_3.repositories.PartRepository;
import org.springframework.stereotype.Service;

import javax.persistence.SecondaryTable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of the part service.
 * Uses the part repository to interact with the data store.
 */
@Service
public class PartServiceImpl implements PartService {

    private final PartRepository partRepository;

    public PartServiceImpl(PartRepository partRepository) {
        this.partRepository = partRepository;
    }

    @Override
    public Part findById(Integer id) {
        if(partRepository.findById(id) == null){
            throw new PartNotFoundException(id);
        }
        return partRepository.findById(id).get();
    }

    @Override
    public Collection<Part> findAll() {
        return partRepository.findAll();
    }

    @Override
    public Part add(Part entity) {
        return partRepository.save(entity);
    }

    @Override
    public Part update(Part entity) {
        return partRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        Part part = partRepository.findById(id).get();
        Set<Movie> movies = part.getMovies();
        Set<Part> newParts;

        for(Movie m : movies){
            newParts = m.getParts();
            newParts.remove(part);
            m.setParts(newParts);
        }
        partRepository.delete(part);
    }

    @Override
    public boolean exists(int id) {
        return partRepository.existsById(id);
    }
}
