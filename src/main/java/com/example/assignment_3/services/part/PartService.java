package com.example.assignment_3.services.part;

import com.example.assignment_3.models.Part;
import com.example.assignment_3.services.CrudService;

/**
 * Service for the part domain class
 * Providing basic CRUD functionality
 */
public interface PartService extends CrudService<Part, Integer> {
    /**
     * Checks if part exists
     * @param id part_id
     * @return true||false
     */
    boolean exists(int id);
}
