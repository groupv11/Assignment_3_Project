package com.example.assignment_3.services.franchise;

import com.example.assignment_3.models.Franchise;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.services.CrudService;

import java.util.Set;

/**
 * Service for the franchise domain class
 * Providing basic CRUD functionality
 */
public interface FranchiseService extends CrudService<Franchise, Integer> {
    /**
     * Checks if franchise exists
     * @param id franchise_id
     * @return true||false
     */
    boolean exists(int id);

    /**
     * Updates all movies with new franchise_id
     * @param newMovies set of movie_id
     * @param id franchise_id
     */
    void updateAllMovies(int [] newMovies, int id);
    }
