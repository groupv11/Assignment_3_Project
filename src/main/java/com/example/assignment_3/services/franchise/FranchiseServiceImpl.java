package com.example.assignment_3.services.franchise;

import com.example.assignment_3.exceptions.FranchiseNotFoundException;
import com.example.assignment_3.models.Franchise;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.repositories.FranchiseRepository;
import com.example.assignment_3.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of the franchise service.
 * Uses the franchise repository to interact with the data store.
 */
@Service
public class FranchiseServiceImpl implements FranchiseService{

    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    @Override
    public Franchise findById(Integer id) {
        if(franchiseRepository.findById(id) == null){
            throw new FranchiseNotFoundException(id);
        }
        return franchiseRepository.findById(id).get();
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        if(franchiseRepository.existsById(id)){
            Franchise franchise = franchiseRepository.findById(id).get();
            franchise.getMovies().forEach(s -> s.setFranchise(null));
            franchiseRepository.delete(franchise);
        }
    }


    @Override
    public void updateAllMovies(int [] newMoviesIds, int id){
        Franchise franchise = franchiseRepository.findById(id).get();
        Set<Movie> movies = new HashSet<>();
        for(Integer ids : newMoviesIds){
            Movie m = movieRepository.findById(ids).get();
            m.setFranchise(franchise);
            movieRepository.save(m);
            movies.add(m);
        }
        franchise.setMovies(movies);
        franchiseRepository.save(franchise);
    }

    @Override
    public boolean exists(int id) {
        return franchiseRepository.existsById(id);
    }
}
