package com.example.assignment_3.services;

import java.util.Collection;

public interface CrudService <T,ID> {
    /**
     * Finds object by id
     * @param id any id
     * @return Object
     */
    T findById(ID id);

    /**
     * Finds all objects from database
     * @return objects
     */
    Collection<T> findAll();

    /**
     * Add object
     * @param entity any object
     */
    T add(T entity);

    /**
     * Update object
     * @param entity any object
     */
    T update(T entity);

    /**
     * Delete object by given id
     * @param id any id
     */
    void deleteById(ID id);
}